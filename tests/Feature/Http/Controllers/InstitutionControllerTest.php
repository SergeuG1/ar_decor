<?php

namespace Tests\Feature\Http\Controllers;

use App\Institution;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\InstitutionController
 */
class InstitutionControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_behaves_as_expected()
    {
        $institutions = factory(Institution::class, 3)->create();

        $response = $this->get(route('institution.index'));
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\InstitutionController::class,
            'store',
            \App\Http\Requests\InstitutionStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves()
    {
        $institution = $this->faker->word;

        $response = $this->post(route('institution.store'), [
            'institution' => $institution,
        ]);

        $institutions = Institution::query()
            ->where('institution', $institution)
            ->get();
        $this->assertCount(1, $institutions);
        $institution = $institutions->first();
    }


    /**
     * @test
     */
    public function show_behaves_as_expected()
    {
        $institution = factory(Institution::class)->create();

        $response = $this->get(route('institution.show', $institution));
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\InstitutionController::class,
            'update',
            \App\Http\Requests\InstitutionUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_behaves_as_expected()
    {
        $institution = factory(Institution::class)->create();
        $institution = $this->faker->word;

        $response = $this->put(route('institution.update', $institution), [
            'institution' => $institution,
        ]);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_responds_with()
    {
        $institution = factory(Institution::class)->create();

        $response = $this->delete(route('institution.destroy', $institution));

        $response->assertOk();

        $this->assertDeleted($institution);
    }
}
