<?php

namespace Tests\Feature\Http\Controllers;

use App\Payment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\PaymentController
 */
class PaymentControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_behaves_as_expected()
    {
        $payments = factory(Payment::class, 3)->create();

        $response = $this->get(route('payment.index'));
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\PaymentController::class,
            'store',
            \App\Http\Requests\PaymentStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves()
    {
        $payment = $this->faker->word;

        $response = $this->post(route('payment.store'), [
            'payment' => $payment,
        ]);

        $payments = Payment::query()
            ->where('payment', $payment)
            ->get();
        $this->assertCount(1, $payments);
        $payment = $payments->first();
    }


    /**
     * @test
     */
    public function show_behaves_as_expected()
    {
        $payment = factory(Payment::class)->create();

        $response = $this->get(route('payment.show', $payment));
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\PaymentController::class,
            'update',
            \App\Http\Requests\PaymentUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_behaves_as_expected()
    {
        $payment = factory(Payment::class)->create();
        $payment = $this->faker->word;

        $response = $this->put(route('payment.update', $payment), [
            'payment' => $payment,
        ]);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_responds_with()
    {
        $payment = factory(Payment::class)->create();

        $response = $this->delete(route('payment.destroy', $payment));

        $response->assertOk();

        $this->assertDeleted($payment);
    }
}
