<?php

namespace Tests\Feature\Http\Controllers;

use App\GeoPosition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\GeoPositionController
 */
class GeoPositionControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_behaves_as_expected()
    {
        $geoPositions = factory(GeoPosition::class, 3)->create();

        $response = $this->get(route('geo-position.index'));
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\GeoPositionController::class,
            'store',
            \App\Http\Requests\GeoPositionStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves()
    {
        $geoPosition = $this->faker->word;

        $response = $this->post(route('geo-position.store'), [
            'geoPosition' => $geoPosition,
        ]);

        $geoPositions = GeoPosition::query()
            ->where('geoPosition', $geoPosition)
            ->get();
        $this->assertCount(1, $geoPositions);
        $geoPosition = $geoPositions->first();
    }


    /**
     * @test
     */
    public function show_behaves_as_expected()
    {
        $geoPosition = factory(GeoPosition::class)->create();

        $response = $this->get(route('geo-position.show', $geoPosition));
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\GeoPositionController::class,
            'update',
            \App\Http\Requests\GeoPositionUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_behaves_as_expected()
    {
        $geoPosition = factory(GeoPosition::class)->create();
        $geoPosition = $this->faker->word;

        $response = $this->put(route('geo-position.update', $geoPosition), [
            'geoPosition' => $geoPosition,
        ]);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_responds_with()
    {
        $geoPosition = factory(GeoPosition::class)->create();

        $response = $this->delete(route('geo-position.destroy', $geoPosition));

        $response->assertOk();

        $this->assertDeleted($geoPosition);
    }
}
