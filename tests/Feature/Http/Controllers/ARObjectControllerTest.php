<?php

namespace Tests\Feature\Http\Controllers;

use App\ARObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\ARObjectController
 */
class ARObjectControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_behaves_as_expected()
    {
        $aRObjects = factory(ARObject::class, 3)->create();

        $response = $this->get(route('a-r-object.index'));
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\ARObjectController::class,
            'store',
            \App\Http\Requests\ARObjectStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves()
    {
        $aRObject = $this->faker->word;

        $response = $this->post(route('a-r-object.store'), [
            'aRObject' => $aRObject,
        ]);

        $aRObjects = ARObject::query()
            ->where('aRObject', $aRObject)
            ->get();
        $this->assertCount(1, $aRObjects);
        $aRObject = $aRObjects->first();
    }


    /**
     * @test
     */
    public function show_behaves_as_expected()
    {
        $aRObject = factory(ARObject::class)->create();

        $response = $this->get(route('a-r-object.show', $aRObject));
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\ARObjectController::class,
            'update',
            \App\Http\Requests\ARObjectUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_behaves_as_expected()
    {
        $aRObject = factory(ARObject::class)->create();
        $aRObject = $this->faker->word;

        $response = $this->put(route('a-r-object.update', $aRObject), [
            'aRObject' => $aRObject,
        ]);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_responds_with()
    {
        $aRObject = factory(ARObject::class)->create();

        $response = $this->delete(route('a-r-object.destroy', $aRObject));

        $response->assertOk();

        $this->assertDeleted($aRObject);
    }
}
