@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Тестовая форма для объекта с id={{$id}}</div>

                    <div class="card-body">
                        <form action="">
                            <div class="form-group mt-2">
                                <input type="text" class="form-control w-100" placeholder="Имя">
                            </div>
                            <div class="form-group mt-2">
                                <input type="email" class="form-control w-100" placeholder="Email">
                            </div>
                            <button class="btn btn-primary">Отправить данные</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
