@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    test
                        <user-institution-list></user-institution-list>
                        <br>
                        <passport-clients></passport-clients>
                        <br>
                        <hr>
                        <passport-authorized-clients></passport-authorized-clients>
                        <br>
                        <hr>
                        <passport-personal-access-tokens></passport-personal-access-tokens>
                    You are logged in!


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
