/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import store from './store'

import YmapPlugin from 'vue-yandex-maps'



const settings = {
    apiKey: '',
    lang: 'ru_RU',
    coordorder: 'latlong',
    version: '2.1'
}

Vue.use(YmapPlugin, settings)

import {BootstrapVue, IconsPlugin} from 'bootstrap-vue';
// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

import VJsoneditor from 'v-jsoneditor';

Vue.use(VJsoneditor);


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component('institutions-map', require('./components/InstitutionsMap').default);
/*Vue.component('example-component', require('./components/ExampleComponent.vue').default);*/

Vue.component('institutions-table', require('./components/admin/InstitutionsTable.vue').default);
Vue.component('markers-table', require('./components/admin/MakersTable.vue').default);
Vue.component('ar-objects-table', require('./components/admin/AR_ObjectsTable.vue').default);
Vue.component('maker-uploader', require('./components/MarkerUploader.vue').default);

Vue.component('user-institution-create', require('./components/manager/institution/InstitutionCreate.vue').default);
Vue.component('user-institution-list', require('./components/manager/institution/InstitiutionList.vue').default);
Vue.component('user-marker-list', require('./components/manager/marker/MarkerList.vue').default);
Vue.component('user-marker-create', require('./components/manager/marker/MarkerCreate.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.filter('pretty', function (value) {
    return JSON.stringify(JSON.parse(value), null, 2);
})

const app = new Vue({
    el: '#app',
    store,
})
