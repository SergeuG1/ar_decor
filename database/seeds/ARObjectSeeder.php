<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ARObjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\ARObject::truncate();
        // factory(\App\ARObject::class, 5)->create();

        \App\ARObject::create([
            'title' => "Объект 1",
            'description' => "Объект - картинка из дистанционного хранилища",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Image,
            'object_path' => json_encode([
                "name" => "8TONkVZz8zQ",
                "url" => 'https://sun9-59.userapi.com/c855136/v855136322/1d178f/8TONkVZz8zQ.jpg'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 2",
            'description' => "Объект - звук",

            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Audio,
            'object_path' => json_encode([
                "name" => "image",
                "url" => '/test_objects/voice.mp3'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 3",
            'description' => "Объект - фильм",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Video,
            'object_path' => json_encode([
                "name" => "film",
                "url" => '/test_objects/film.mp4'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 4",
            'description' => "Объект - текст",

            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Text,
            'object_path' => json_encode([
            ]),
            'object_settings' => json_encode([
                "color" => "#ff0000",
                "font_size" => "14",
                "text" => "simple text"
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 5",
            'description' => "Объект - картинка из локального хранилища",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Image,
            'object_path' => json_encode([
                "name" => "logo",
                "url" => '/test_objects/logo.png'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 6",
            'description' => "Объект - картинка",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Image,
            'object_path' => json_encode([
                "name" => "vinnik",
                "url" => 'https://www.bestiary.us/files/images/vinnik.jpg'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 7",
            'description' => "Объект - 3d cube",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::AssetBundle,
            'object_path' => json_encode([
                "name" => "cube",
                "url" => '/test_objects/cube'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

            \App\ARObject::create([
            'title' => "Объект 8",
            'description' => "Объект - 3d revista",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::AssetBundle,
            'object_path' => json_encode([
                "name" => "revista",
                "url" => '/test_objects/revista'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 9",
            'description' => "Объект - film",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Video,
            'object_path' => json_encode([
                "name" => "film",
                "url" => '/test_objects/film.mp4'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 10",
            'description' => "Объект - фильм videoplayback.mp4",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Video,
            'object_path' => json_encode([
                "name" => "videoplayback",
                "url" => '/test_objects/videoplayback.mp4'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 11",
            'description' => "Объект - music.mp3",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Audio,
            'object_path' => json_encode([
                "name" => "music",
                "url" => '/test_objects/music.mp3'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 12",
            'description' => "Объект - картинка из дистанционного хранилища",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Image,
            'object_path' => json_encode([
                "name" => "UC6VbQrEtv0",
                "url" => 'https://sun9-3.userapi.com/c857724/v857724206/212aa4/UC6VbQrEtv0.jpg'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 13",
            'description' => "Объект - картинка из дистанционного хранилища",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Image,
            'object_path' => json_encode([
                "name" => "op4LTYALiIQ",
                "url" => 'https://sun9-50.userapi.com/c854024/v854024983/244a43/op4LTYALiIQ.jpg'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
        'title' => "Объект 14",
        'description' => "Объект - картинка из дистанционного хранилища",
        'transform' => json_encode([
            "position" => [
                "x" => 0.0,
                "y" => 0.0,
                "z" => 0.0,
            ],
            "rotation" => [
                "x" => 0.0,
                "y" => 0.0,
                "z" => 0.0,
            ],
            "scale" => [
                "x" => 0.0,
                "y" => 0.0,
                "z" => 0.0,
            ]
        ]),
        'object_type' => \App\Enums\ObjectTypeEnum::Image,
        'object_path' => json_encode([
            "name" => "AFKYPzAim-s",
            "url" => 'https://sun9-65.userapi.com/c854024/v854024983/244a31/AFKYPzAim-s.jpg'
        ]),
        'object_settings' => json_encode([
        ]),

    ]);

        \App\ARObject::create([
            'title' => "Объект 15",
            'description' => "Объект - картинка из локального хранилища",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Image,
            'object_path' => json_encode([
                "name" => "material",
                "url" => '/test_objects/material.png'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 16",
            'description' => "Объект - картинка из дистанционного хранилища",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Image,
            'object_path' => json_encode([
                "name" => "5ttvuSSqtOo",
                "url" => 'https://sun9-70.userapi.com/c857036/v857036007/13eea3/5ttvuSSqtOo.jpg'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 17",
            'description' => "Объект - белая акула",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 270.0,
                    "y" => 180.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.2,
                    "y" => 0.2,
                    "z" => 0.2,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::AnimatedAssetBundle,
            'object_path' => json_encode([
                "name" => "whiteshark.prefab",
                "url" => '/test_objects/whiteshark'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);


        \App\ARObject::create([
            'title' => "Объект 18",
            'description' => "Объект - 3d из локального хранилища",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::AssetBundle,
            'object_path' => json_encode([
                "name" => "burger",
                "url" => '/test_objects/burger'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

        \App\ARObject::create([
            'title' => "Объект 19 КНОПКА",
            'description' => "Объект - 3d КНОПКА!!",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 50.0,
                    "y" => 50.0,
                    "z" => 50.0,
                ]
            ]),
                'object_type' => \App\Enums\ObjectTypeEnum::AssetBundle,
            'object_path' => json_encode([
                "name" => "button",
                "url" => '/test_objects/button'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);


        \App\ARObject::create([
            'title' => "Объект 20 test",
            'description' => "Объект - картика",
            'transform' => json_encode([
                "position" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "rotation" => [
                    "x" => 0.0,
                    "y" => 0.0,
                    "z" => 0.0,
                ],
                "scale" => [
                    "x" => 50.0,
                    "y" => 50.0,
                    "z" => 50.0,
                ]
            ]),
            'object_type' => \App\Enums\ObjectTypeEnum::Image,
            'object_path' => json_encode([
                "name" => "6",
                "url" => 'https://webformyself.com/wp-content/uploads/2017/132/6.png'
            ]),
            'object_settings' => json_encode([
            ]),

        ]);

              \App\ARObject::create([
                  'title' => "Объект 21 test",
                  'description' => "Объект - картика",
                  'transform' => json_encode([
                      "position" => [
                          "x" => 0.0,
                          "y" => 0.0,
                          "z" => 0.0,
                      ],
                      "rotation" => [
                          "x" => 0.0,
                          "y" => 0.0,
                          "z" => 0.0,
                      ],
                      "scale" => [
                          "x" => 50.0,
                          "y" => 50.0,
                          "z" => 50.0,
                      ]
                  ]),
                  'object_type' => \App\Enums\ObjectTypeEnum::Image,
                  'object_path' => json_encode([
                      "name" => "pTP6veP",
                      "url" => 'https://i.imgur.com/pTP6veP.jpg'
                  ]),
                  'object_settings' => json_encode([
                  ]),

              ]);
    }
}
