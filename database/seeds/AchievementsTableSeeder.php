<?php

use Illuminate\Database\Seeder;

class AchievementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Achievement::create([
            'title'=>"Тестовое достижение 1",
            'description'=>"То самое тестовое достижение",
            'ach_image_url'=>"-",
            'trigger_type'=>\App\Trigger::get(1)->id,
            'trigger_value'=>100,
            'prize_description'=>"тестовое описание приза",
            'prize_image_url'=>"-",
            'position'=>0,
            'is_active'=>true
        ]);
    }
}
