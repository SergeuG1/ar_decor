<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class InstitutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Institution::truncate();
        // factory(\App\Institution::class, 5)->create();
        \App\Institution::create([

            'title' => "Кафедра Компьютерных технологий",
            'description' => "Учебное помещение, задекорированное студентами",
            'image' => '/test_objects/logo.png',
            'type' => \App\Enums\InstitutionTypeEnum::Floor,
            'is_active' => true,
            'position' => 1,
            'assigned_to_id' => null,
            'geo_coords' => json_encode([
                "latitude" => 48.005763,
                "longitude" => 37.796678,
                "radius" => 20,//метры
                "fill" => [

                ],
                "stroke" => [

                ]
            ]),
            "created_at" => \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),

        ]);
    }
}
