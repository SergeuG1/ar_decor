<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Institution;
use Faker\Generator as Faker;

$factory->define(Institution::class, function (Faker $faker) {

    return [
        'title' => $faker->sentence(4),
        'description' => $faker->text,
        'image' => $faker->imageUrl(),
        'type' => $faker->randomElement(
            \App\Enums\InstitutionTypeEnum::toArray()
        ),
        'is_active' => $faker->boolean,
        'position' => $faker->numberBetween(0,100),
        'assigned_to_id' => null,
        'geo_coords' => json_encode([
            "latitude" => 0.0,
            "longitude" => 0.0,
            "radius"=>20,//метры
            "fill"=>[

            ],
            "stroke"=>[

            ]
        ]),
    ];
});
