<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Marker;
use Faker\Generator as Faker;

$factory->define(Marker::class, function (Faker $faker) {
    return [
        'sticker_image' => $faker->imageUrl(),
        'institution_id' => factory(\App\Institution::class),
        'image_set' => json_encode([
            [
                "url"=>$faker->imageUrl()
            ]
        ]),
        'is_active' => $faker->boolean,
        'title' => $faker->sentence(4),
        'description' => $faker->text,
        'action_link' => "https://example.com/action/1",
        'position' => $faker->numberBetween(0,100),
    ];
});
