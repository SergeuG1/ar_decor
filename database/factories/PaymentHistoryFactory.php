<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PaymentHistory;
use Faker\Generator as Faker;

$factory->define(PaymentHistory::class, function (Faker $faker) {
    return [
        'transferred_money' => $faker->numberBetween(0,1000),
    ];
});
