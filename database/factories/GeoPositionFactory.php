<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\GeoPosition;
use Faker\Generator as Faker;

$factory->define(GeoPosition::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(4),
        'description' => $faker->text,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
    ];
});
