<?php

namespace App\Events;

use App\Achievement;
use App\Trigger;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;

class AchievementEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $trigger_type;
    public $trigger_value;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($trigger, $value)
    {

        $tmp_trigger = $trigger instanceof Trigger ? $trigger->id : $trigger;

        $this->trigger_type = $tmp_trigger;
        $this->trigger_value = $value;
    }

}
