<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Institution extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->image,
            'type' => $this->type,
            'is_active' => $this->is_active,
            'creator_id' => $this->creator_id,
            //'assigned_to_id' => $this->assigned_to_id,
            'geo_coords' => json_decode($this->geo_coords),
            'position' => $this->position,
            'markers'=>(new MarkerCollection($this->markers))//["data"],

        ];
    }
}
