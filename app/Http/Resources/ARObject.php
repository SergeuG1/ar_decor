<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ARObject extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            //'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            //'marker_id' => $this->marker_id,
            'transform' => json_decode($this->transform),
            'object_type' => $this->object_type,
            'object_path' => json_decode($this->object_path),
            'object_settings' => json_decode($this->object_settings),

        ];
    }
}
