<?php

namespace App\Http\Controllers;

use App\Http\Requests\InstitutionStoreRequest;
use App\Http\Requests\InstitutionUpdateRequest;
use App\Http\Resources\Institution as InstitutionResource;
use App\Http\Resources\InstitutionCollection;
use App\Institution;
use App\Marker;
use Illuminate\Http\Request;

class InstitutionController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\InstitutionCollection
     */
    public function all(Request $request)
    {
        $institutions = Institution::with(["markers", "markers.aRObject"])
            ->orderBy('position', 'DESC')
            ->get();

        $deleted_institutions = Institution::onlyTrashed()->with(["markers", "markers.arobject"])->get();

        return response()
            ->json([
                'items' => $institutions,
                'deleted_items' => $deleted_institutions,
            ], 200);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\InstitutionCollection
     */
    public function index(Request $request)
    {
        $institutions = Institution::with(["markers", "markers.arobject"])
            ->orderBy('position', 'DESC')
            ->simplePaginate(config('app.institution_per_page'));

        return new InstitutionCollection($institutions);
    }

    /**
     * @param \App\Http\Requests\InstitutionStoreRequest $request
     * @return \App\Http\Resources\Institution
     */
    public function store(InstitutionStoreRequest $request)
    {
        $institution = Institution::create($request->all());

        return new InstitutionResource($institution);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Institution $institution
     * @return \App\Http\Resources\Institution
     */
    public function show(Request $request, Institution $institution)
    {
        return new InstitutionResource(Institution::with(["markers", "markers.arobject"])->where('id', $institution->id)->first());

    }

    /**
     * @param \App\Http\Requests\InstitutionUpdateRequest $request
     * @param \App\Institution $institution
     * @return \App\Http\Resources\Institution
     */
    public function update(InstitutionUpdateRequest $request, Institution $institution)
    {
        $institution->update([
            'title'=>$request->title,
            'description'=>$request->description,
            'image'=>$request->image,
            'type'=>$request->type,
            'is_active'=>$request->is_active,
            'position'=>$request->position,
            'assigned_to_id'=>$request->assigned_to_id,
            'geo_coords'=>$request->geo_coords,
        ]);

        return new InstitutionResource($institution);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Institution $institution
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $institution = Institution::find($id);
        $institution->delete();

        return response()->noContent(200);
    }

    public function restore($id)
    {
        $institution = Institution::onlyTrashed()->where('id', $id)->restore();

        return response()
            ->json([
                "message" => "Заведение восстановлено",
                "status" => 200,
            ]);
    }

    public function save(Request $request, $id)
    {
        $param = $request->get("param");
        $value = $request->get("value");

        $institution = Institution::find($id);
        $institution[$param] = $value;
        $institution->save();

        return response()
            ->json([
                "message" => "Изменения сохранены",
            ], 200);
    }

    public function duplication($id)
    {
        $institution = Institution::find($id);

        $institution = $institution->replicate();
        $institution->save();

        $institution->title = sprintf("Копия %s",
            $id
        );

        $institution->save();

        return response()
            ->json([
                "message" => "Успешно продублировано"
            ]);

    }


    public function selfCreatedList(Request $request)
    {
        $userId = $request->get("user_id");

        return response()
            ->json(Institution::where("creator_id", $userId)->orderBy("id","desc")->paginate(env("APP_PAGINATE_COUNT")));
    }
}
