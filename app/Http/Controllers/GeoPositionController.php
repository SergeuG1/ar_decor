<?php

namespace App\Http\Controllers;

use App\ARObject;
use App\GeoPosition;
use App\Http\Requests\GeoPositionStoreRequest;
use App\Http\Requests\GeoPositionUpdateRequest;
use App\Http\Resources\ARObjectCollection;
use App\Http\Resources\GeoPosition as GeoPositionResource;
use App\Http\Resources\GeoPositionCollection;
use Illuminate\Http\Request;

class GeoPositionController extends Controller
{

    public function inRange(Request $request){

        $request->validate([
            'latitude'=>'required',
            'longitude'=>'required',
            'radius'=>'required'
        ]);

        $geo_positions = GeoPosition::getNearestPoints(
            $request->latitude,
            $request->longitude,
            $request->radius

        );//->simplePaginate(config('app.arobject_per_page'));

        return new GeoPositionCollection($geo_positions);
    }
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\GeoPositionCollection
     */
    public function index(Request $request)
    {
        $geoPositions = GeoPosition::all();

        return new GeoPositionCollection($geoPositions);
    }

    /**
     * @param \App\Http\Requests\GeoPositionStoreRequest $request
     * @return \App\Http\Resources\GeoPosition
     */
    public function store(GeoPositionStoreRequest $request)
    {
        $geoPosition = GeoPosition::create($request->all());

        return new GeoPositionResource($geoPosition);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\GeoPosition $geoPosition
     * @return \App\Http\Resources\GeoPosition
     */
    public function show(Request $request, GeoPosition $geoPosition)
    {
        return new GeoPositionResource($geoPosition);
    }

    /**
     * @param \App\Http\Requests\GeoPositionUpdateRequest $request
     * @param \App\GeoPosition $geoPosition
     * @return \App\Http\Resources\GeoPosition
     */
    public function update(GeoPositionUpdateRequest $request, GeoPosition $geoPosition)
    {
        $geoPosition->update([]);

        return new GeoPositionResource($geoPosition);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\GeoPosition $geoPosition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, GeoPosition $geoPosition)
    {
        $geoPosition->delete();

        return response()->noContent(200);
    }
}
