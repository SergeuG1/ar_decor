<?php

namespace App\Http\Controllers;

use App\ARObject;
use App\Http\Requests\ARObjectStoreRequest;
use App\Http\Requests\ARObjectUpdateRequest;
use App\Http\Resources\ARObject as ARObjectResource;
use App\Http\Resources\ARObjectCollection;
use Illuminate\Http\Request;

class ARObjectController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\ARObjectCollection
     */
    public function all(Request $request)
    {
        $aRObjects = ARObject::all();
        $deleted_aRObjects = ARObject::onlyTrashed()->get();

        return response()
            ->json([
                'items' => new ARObjectCollection($aRObjects),
                'deleted_items' => new ARObjectCollection($deleted_aRObjects),
            ], 200);
//        return new ARObjectCollection($aRObjects);
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\ARObjectCollection
     */
    public function index(Request $request)
    {
        $aRObjects = ARObject::simplePaginate(config('app.arobject_per_page'));

        return new ARObjectCollection($aRObjects);
    }

    /**
     * @param \App\Http\Requests\ARObjectStoreRequest $request
     * @return \App\Http\Resources\ARObject
     */
    public function store(ARObjectStoreRequest $request)
    {
        $aRObject = ARObject::create($request->all());

        return new ARObjectResource($aRObject);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\ARObject $aRObject
     * @return \App\Http\Resources\ARObject
     */
    public function show(Request $request, ARObject $aRObject)
    {
        return new ARObjectResource($aRObject);
    }

    /**
     * @param \App\Http\Requests\ARObjectUpdateRequest $request
     * @param \App\ARObject $aRObject
     * @return \App\Http\Resources\ARObject
     */
    public function update(ARObjectUpdateRequest $request, ARObject $aRObject)
    {
        $aRObject->update([]);

        return new ARObjectResource($aRObject);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\ARObject $aRObject
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aRObject = ARObject::find($id);
        $aRObject->delete();

        return response()->noContent(200);
    }

    public function restore($id)
    {
        $aRObject = ARObject::onlyTrashed()->where('id', $id)->restore();

        return response()
            ->json([
                "message" => "Объект восстановлен",
                "status" => 200,
            ]);
    }
    public function save(Request $request, $id)
    {
        $param = $request->get("param");
        $value = $request->get("value");

        $aRObject = ARObject::find($id);
        $aRObject[$param] = $value;
        $aRObject->save();

        return response()
            ->json([
                "message" => "Изменения сохранены",
            ],200);
    }
}
