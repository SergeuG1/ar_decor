<?php

namespace App\Http\Controllers;

use App\Achievement;
use App\Http\Resources\AchievementCollection;
use App\Http\Resources\User;
use App\Stat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AchievementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->isJson()) {
            return response()
                ->json(
                    Achievement::paginate(20)
                );
        }
    }

    public function complete(Request $request)
    {
        $user = new User(Auth::user());

        return response()
            ->json($user->achievements()->paginate(env("APP_PAGINATE_COUNT")));


    }

    public function progress(Request $request, $id)
    {
        $user = new User(Auth::user());

        $achievement = Achievement::where("id", $id)->first();

        if (is_null($achievement))
            return response()->json([
                "message" => "Достижение не найдено!"
            ]);

        $stat = Stat::where("stat_type", $achievement->trigger_type)
            ->where("user_id", $user->id)
            ->first();

        $current = (is_null($stat) ? 0 : $stat->stat_value);
        $needded = $achievement->trigger_value;

        return response()
            ->json([
                "achievement" => new \App\Http\Resources\Achievement($achievement),
                "current_progress" => $current,
                "needed_progress" => $needded,
                "is_complete" => $current >= $needded
            ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Achievement $achievement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $achievement = Achievement::where("id", $id)->first();

        if (is_null($achievement))
            return response()->json([
                "message" => "Достижение не найдено!"
            ]);

        return response()->json(new \App\Http\Resources\Achievement($achievement));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Achievement $achievement
     * @return \Illuminate\Http\Response
     */
    public function edit(Achievement $achievement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Achievement $achievement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Achievement $achievement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Achievement $achievement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Achievement $achievement)
    {
        //
    }
}
