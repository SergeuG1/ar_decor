<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ARObjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'geo_coords' => 'required|json',
            'transform' => 'required|json',
            'object_type' => 'required|integer',
            'object_path' => 'required|json',
            'object_settings' => 'required|json',
        ];
    }
}
