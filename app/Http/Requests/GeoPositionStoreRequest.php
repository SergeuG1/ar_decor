<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeoPositionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:200',
            'description' => 'required|string|max:1000',
            'prev_image_set' => 'required|json',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'deleted_at' => 'nullable',
        ];
    }
}
