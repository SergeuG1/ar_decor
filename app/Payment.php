<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment',
        'current_money',
        'min_money_limit',
        'money_per_day',
        'user_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'current_money' => 'double',
        'min_money_limit' => 'double',
        'money_per_day' => 'double',
        'user_id' => 'integer',
    ];

    public function history(){
        return $this->hasMany(PaymentHistory::class,"user_id","user_id");
    }


}
