<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class ObjectTypeEnum extends Enum
{
    const Image =   0;
    const Video =   1;
    const Model = 2;
    const Audio = 3;
    const AssetBundle = 4;// скорее всего только модель
    const Text = 5;
    const Button = 6;
    const AnimatedAssetBundle = 7;
}
