<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScanedMarker extends Model
{
    protected $fillable = [
        "marker_id",
        "user_id"
    ];
}
