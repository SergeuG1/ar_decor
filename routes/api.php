<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/auth/actions/{id}", function ($marker_id) {
    return response()->json([
        "message" => "Action AUTH from $marker_id",
        "url" => "http://likholetov.beget.tech/example/$marker_id"
    ]);
})->where(["id" => "[0-9]+"])
    ->middleware(["auth:api", "triggers"])
    ->name("auth.actions");

Route::get("/actions/{id}", function (Request $request, $marker_id) {
    if ($request->hasHeader("Authorization"))
        return response()->redirectToRoute("auth.actions", $marker_id);

    return response()->json([
        "message" => "Action SIMPLE from $marker_id",
        "url" => "http://likholetov.beget.tech/example/$marker_id"
    ]);
})->where(["id" => "[0-9]+"]);

Route::post('/geo-position/inrange/', 'GeoPositionController@inRange');

Route::post('institution/creator', 'InstitutionController@selfCreatedList');
Route::get('institution/duplicate/{id}', 'InstitutionController@duplication');

Route::post('marker/creator', 'MarkerController@selfCreatedList');
Route::post('a-r-object/creator', 'ARObjectController@selfCreatedList');
Route::post('achievements/creator', 'AchievementController@selfCreatedList');

Route::apiResource('geo-position', 'GeoPositionController');

Route::apiResource('institution', 'InstitutionController');


Route::apiResource('marker', 'MarkerController');

Route::apiResource('a-r-object', 'ARObjectController');

Route::apiResource('payment', 'PaymentController');

Route::apiResource('payment-history', 'PaymentHistoryController');

Route::post('auth/signup', 'API\AuthController@signup');
Route::post('auth/signup_by_phone', 'API\AuthController@signupByPhone');

Route::match(["get", "post"], 'auth/me', 'API\AuthController@getMe')->middleware('auth:api');

Route::get('achievements', 'AchievementController@index');
Route::get('achievements/complete', 'AchievementController@complete')->middleware('auth:api');
Route::get('achievements/{id}', 'AchievementController@show')->where(["id" => "[0-9]+"]);
Route::get('achievements/progress/{id}', 'AchievementController@progress')->where(["id" => "[0-9]+"])->middleware('auth:api');

Route::get('payments', 'PaymentController@payments')->middleware('auth:api');
Route::get('payments/history', 'PaymentController@history')->middleware('auth:api');

Route::get('auth/test', function () {
    event(new \App\Events\AchievementEvent(1, 10));
    event(new \App\Events\AchievementEvent(\App\Trigger::get("Level"), 10));
    return "success";
})->middleware('auth:api');



Route::post('auth/token', 'API\AuthController@authenticate');
Route::post('auth/token_by_phone', 'API\AuthController@authenticateByPhone');

Route::post('auth/refresh', 'API\AuthController@refreshToken');

Route::fallback(function () {
    return response()->json([
        "message" => "Данный маршрут не поддерживается"
    ]);
});
